package com.jessetrem1333772.calculator;
/**
 * Custom exception thrown by Calculator class when an equation is invalid
 * @author Jesse Tremblay
 *
 */
public class OffBalanceOperatorsException extends RuntimeException {
    //Parameterless Constructor
    public OffBalanceOperatorsException() {}

    //Constructor that accepts a message
    public OffBalanceOperatorsException(String message)
    {
       super(message);
    }
}
