package com.jessetrem1333772.calculator;

import java.util.ArrayDeque;
import java.util.NoSuchElementException;
/**
 * Class used to calculate an equation respecting the mathematical
 * order of operations
 * @author Jesse Tremblay
 *
 */
public class Calculator {
	String numberRegex;
	/**
	 * no parameter constructor for a calculator object
	 */
	public Calculator(){
		numberRegex = "[0-9]+";
	}
	
	/**
	 * Method that takes an array deque representing an infix notation equation
	 * and converts it to post fix
	 * @param equation the infix equation
	 * @return and ArrayDeque representing the postfix equation
	 */
	public ArrayDeque<String> convertToPolishNotation(ArrayDeque<String> equation){
		ArrayDeque<String> operatorStack = new ArrayDeque<String>();
		ArrayDeque<String> result = new ArrayDeque<String>();
		String equatorPeek;
		while(!equation.isEmpty()){
			String position = equation.pop();
			if( position.matches(numberRegex)){
				result.add(position);
			}else{
				
				if(position.equals(")")){
					do{
						//if this condition is met then there is no opening brace to the closing one and thus throws
						//an exception
						if(operatorStack.isEmpty()){
							throw new OffBalanceOperatorsException("Closing brace found without opening brace");
						}
						equatorPeek = operatorStack.pop();
						if(!equatorPeek.equals("(")){
							result.add(equatorPeek);
						}
						
					}while(!equatorPeek.equals("("));
				}else{
					equatorPeek = operatorStack.peek();
					//possibilities where value of position does not matter
					if(equatorPeek == null || equatorPeek.equals("(") || position.equals("(")){
						operatorStack.push(position);
					}else{
						if(equatorPeek.equals("+") || equatorPeek.equals("-")){
							if(position.equals("+") || position.equals("-")){
								result.add(operatorStack.pop());
								operatorStack.push(position);
							}else{
								operatorStack.push(position);
							}
						}else{
							result.add(operatorStack.pop());
							operatorStack.push(position);
						}
					}
				}
			}
		}
		while(!operatorStack.isEmpty()){
			result.add(operatorStack.pop());
		}
		return result;
	}
	
	/**
	 * Method that calculates the result of a postfix equation represented by an
	 * ArrayDeque
	 * @param postfix the equation to be solved
	 * @return the result of the equation
	 */
	public String calculateResult(ArrayDeque<String> postfix) {
		ArrayDeque<String> operandStack = new ArrayDeque<String>();
		ArrayDeque<String> equationDeque = new ArrayDeque<String>();
		String position;
		int result = 0;
		try{
			while(!postfix.isEmpty()){	
				position = postfix.pop();
				if(position.matches(numberRegex)){
					operandStack.push(position);
				}else{
					equationDeque.push(operandStack.pop());
					equationDeque.push(operandStack.pop());
					switch(position){
						case "+":
							result = Integer.parseInt(equationDeque.pop()) + Integer.parseInt(equationDeque.pop());
							break;
						case "-":
							result = Integer.parseInt(equationDeque.pop()) - Integer.parseInt(equationDeque.pop());
							break;
						case "*":
							result = Integer.parseInt(equationDeque.pop()) * Integer.parseInt(equationDeque.pop());
							break;
						case "/":
							result = Integer.parseInt(equationDeque.pop()) / Integer.parseInt(equationDeque.pop());
							break;
					}
					operandStack.push(result + "");
				}
			}
			if(operandStack.size() > 1){
				throw new OffBalanceOperatorsException("Not enough operators");
			}
				return operandStack.pop();
		}catch(NoSuchElementException nse){ //this exception is thrown whenever there are too many operators
			throw new OffBalanceOperatorsException(nse.getMessage());
		}
	}
	/**
	 * Method that takes a string representation of an equation and converts it
	 * to an ArrayDeque
	 * @param equation the string to be converted into an ArrayDeque
	 * @return the ArrayDeque representing the equation
	 */
	public ArrayDeque<String> stringToEquationDeque(String equation){
		String number = "";
		String nextNum;
		ArrayDeque<String> dequeEquation = new ArrayDeque<String>();
		for (int i = 0; i < equation.length(); i++){
			nextNum = (equation.charAt(i) + "").trim();
			//loops through each character until an operator is hit
			if(nextNum.matches(numberRegex)){
				number += nextNum;
			}else{
				if(!number.equals("")){
					dequeEquation.add(number);
				}
				dequeEquation.add(nextNum);
				number = "";
			}
		}
		//adds the final operand if the equation ends with an operand
		if(!number.equals("")){
			dequeEquation.add(number);
		}
		return dequeEquation;
	}
}
