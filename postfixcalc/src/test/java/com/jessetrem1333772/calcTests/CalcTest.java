/**
 * 
 */
package com.jessetrem1333772.calcTests;

import static org.junit.Assert.*;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.Before;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jessetrem1333772.calculator.Calculator;

import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;

/**
 * @author Jesse
 *
 */
@RunWith(Parameterized.class)
public class CalcTest {
	private final Logger log = LoggerFactory.getLogger(getClass().getName());
    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {     
                 { "2*(2+3)*3", "30" } , {"1+1", "2"}, {"2*2", "4"}, {"2-2", "0"}, {"4/2", "2"}, {"(1+1)*2", "4"}, {"1+1*2", "3"},
                 {"2*2/2", "2"}, {"3*(2/2)", "3"}, {"2+3-1", "4"}, {"2*3-1", "5"}, {"2*(3-1)", "4"}, {"2+2*(3-4)/2", "1"},
                 {"(2-2)*(3-4)/2", "0"}, {"2*2*(3-4)/2", "-2"}
           });
    }

    private String tInput;

    private String tExpected;

    public CalcTest(String input, String expected) {
        tInput= input;
        tExpected= expected;
    }

    @Test
    public void test() {
    	
    	Calculator calc = new Calculator();
    	ArrayDeque<String> equation = calc.stringToEquationDeque(tInput);
    	//log.error(equation.toString());
    	equation = calc.convertToPolishNotation(equation);
    	//log.error(equation.toString());
    	String res = calc.calculateResult(equation);
    	log.error("result: " + res + " expected: " + tExpected);//set to error since config file not found
        assertEquals(tExpected, res);
    }

}
