package com.jessetrem1333772.calcTests;

import static org.junit.Assert.*;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.Before;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jessetrem1333772.calculator.Calculator;
import com.jessetrem1333772.calculator.OffBalanceOperatorsException;

import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class CalcExceptionTest {

	private final Logger log = LoggerFactory.getLogger(getClass().getName());
    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {     
                 { "2+()+3"}, { "2(2+3)*3"}, { "2*(2+3*3"},{ "2*2+3)*3"}, {"2++3"}, {"+2+"}
           });
    }

    private String tInput;

    private String tExpected;

    public CalcExceptionTest(String input) {
        tInput= input;
    }
    //this is the same test as the other test class however I am sending invalid data
    @Test(expected = OffBalanceOperatorsException.class)
    public void test() {	
    	Calculator calc = new Calculator();
    	ArrayDeque<String> equation = calc.stringToEquationDeque(tInput);
    	equation = calc.convertToPolishNotation(equation);
    	String res = calc.calculateResult(equation);
    	log.error("result: " + res + " expected: " + tExpected);//set to error since config file not found
    }
}
